function dateNow() {
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const fullDate = `${year}/${month}/${day}`;
  return fullDate;
}

module.exports = dateNow;
