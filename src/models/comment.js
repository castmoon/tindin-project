const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
  id_class: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Class',
    required: true,
  },
  comment: {
    type: String,
    required: true,
  },
  date_created: {
    type: String,
  },
});

module.exports = mongoose.model('Comment', commentSchema);
