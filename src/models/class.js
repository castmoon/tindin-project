const mongoose = require('mongoose');

const classSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  video: {
    type: String,
    required: true,
  },
  data_init: {
    type: String,
    required: true,
  },
  data_end: {
    type: String,
    required: true,
  },
  date_created: {
    type: String,
  },
  date_updated: {
    type: String,
  },
  total_comments: {
    type: Number,
    default: 0,
  },
});

module.exports = mongoose.model('Class', classSchema);
