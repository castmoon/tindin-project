const { Router } = require('express');
const Class = require('../models/class');
const Comment = require('../models/comment');
const dateNow = require('../functions/date');

const router = Router();

router.get('/', async (req, res) => {
  const classes = await Class.find({});
  if (!classes[0]) {
    res.status(404).json({ message: 'no classes found.' });
  }
  res.status(201).json({ Classes: classes });
});

router.post('/', async (req, res) => {
  try {
    const { name, description, video, data_init, data_end } = req.body;
    const createClass = await Class.create({
      name,
      description,
      video,
      data_init,
      data_end,
    });
    res.status(201).json(createClass);
  } catch (e) {
    res.status(400).json(e);
  }
});

router.get('/:id', async (req, res) => {
  const classById = await Class.findById(req.params.id);
  if (!classById) {
    res.status(404).json({ message: 'Class not found.' });
  }
  res.status(200).json(classById);
});

router.put('/', async (req, res) => {
  try {
    const { id, name, description, video, data_init, data_end } = req.body;
    const date_updated = dateNow();
    if (!id) {
      res
        .status(404)
        .status(400)
        .json({ message: 'id not provided on the body of requisition.' });
    }
    const newClass = await Class.findByIdAndUpdate(
      id,
      {
        name,
        description,
        video,
        data_init,
        data_end,
        date_updated,
      },
      {
        new: true,
      },
    );
    await newClass.save();
    res.status(201).json(newClass);
  } catch (e) {
    res.status(400).json(e);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const classById = await Class.findById(req.params.id);
    if (!classById) {
      res.status(404).json({ message: 'Class not found' });
    }
    await Class.findByIdAndDelete(req.params.id);
    res.status(200).json({ message: 'delete successful' });
  } catch (e) {
    res.status(400).json(e);
  }
});

router.post('/comments', async (req, res) => {
  try {
    const { id_class, comment } = req.body;
    const date_created = dateNow();
    const verifyClass = await Class.findById({
      _id: id_class,
    });
    if (!verifyClass) {
      res.status(404).json({ message: 'class not found.' });
    }
    if (!id_class) {
      res
        .status(404)
        .status(400)
        .json({ message: 'id not provided on the body of requisition.' });
    }
    const addComment = await Comment.create({
      id_class,
      comment,
      date_created,
    });
    verifyClass.total_comments += 1;
    await verifyClass.save();

    res.status(200).json(addComment);
  } catch (e) {
    res.status(400).json(e);
  }
});

router.get('/comments/:id', async (req, res) => {
  try {
    const classComments = await Comment.find({
      id_class: req.params.id,
    });
    if (!classComments[0]) {
      res.status(404).json({ message: 'There are no comments.' });
    }
    res.status(200).json({ classComments });
  } catch (e) {
    res.status(400).json(e);
  }
});

router.delete('/comments/:id', async (req, res) => {
  try {
    const commentById = await Comment.findById(req.params.id);
    const classById = await Class.findById(commentById.id_class);
    if (!commentById) {
      res.status(404).json({ message: 'Comment not found.' });
    }
    await Comment.findByIdAndDelete(req.params.id);
    classById.total_comments -= 1;
    await classById.save();
    res.status(200).json({ message: 'delete successful' });
  } catch (e) {
    res.status(400).json(e);
  }
});

module.exports = router;
