const express = require('express');
require('../database/connection');
const classRouter = require('./controllers/classesController');

const app = express();

app.use(express.json());
app.use('/classes', classRouter);

app.listen(3001, () => console.log('backend started at port 3001'));
