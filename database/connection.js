const moongose = require('mongoose');

moongose.Promise = global.Promise;

moongose
  .connect('mongodb://localhost/tindin', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('Conectado ao MongoDB'))
  .catch((e) => console.log(e));
